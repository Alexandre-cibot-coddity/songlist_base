#core/tests/test_routes.py
import unittest
import json

from core.server import db
from core.server.models import Song
from core.tests.base import BaseTestCase

class TestRoutes(BaseTestCase):
    
    def test_add_new_song(self):
        """ test_for_adding_a_song"""
        with self.client:
            response = self.client.post('/add', 
                data=json.dumps(dict(
                    title = 'Blitzkrieg bop - The Ramones',
                    year = 1976)),
                content_type = 'application/json')
            data = json.loads(response.data.decode())
            self.assertTrue(data['status'] == 'success')
            self.assertTrue(data['message'] == 'song added')
            self.assertTrue(response.content_type == 'application/json')
            self.assertEqual(response.status_code, 200)

    def test_add_song_already_added(self):
        """ test_for_adding_an_already_added_song"""
        with self.client:
            response = self.client.post('/add', 
                data=json.dumps(dict(
                    title = 'Blitzkrieg bop - The Ramones',
                    year = 1976)),
                content_type = 'application/json')
            response = self.client.post('/add', 
                data=json.dumps(dict(
                    title = 'Blitzkrieg bop - The Ramones',
                    year = 1976)),
                content_type = 'application/json')
            data = json.loads(response.data.decode())
            self.assertTrue(data['status'] == 'fail')
            self.assertTrue(data['message'] == 'song already in base')
            self.assertTrue(response.content_type == 'application/json')
            self.assertEqual(response.status_code, 409)

    def test_getall(self):
        """ test_for_get_all_songs """
        with self.client:
            post = self.client.post('/add', 
                data=json.dumps(dict(
                    title = 'Blitzkrieg bop - The Ramones',
                    year = 1976)),
                content_type = 'application/json')
            post = self.client.post('/add', 
                data=json.dumps(dict(
                    title = 'Atomic - Blondie',
                    year = 1979)),
                content_type = 'application/json')
            response = self.client.get('/getlist')
            data = json.loads(response.data.decode())
            self.assertTrue(response.content_type == 'application/json')
            self.assertEqual(response.status_code, 200)
            for i in data:
                self.assertTrue(i['title'])
                self.assertTrue(i['year'])

    def test_getall_emptylist(self):
        """ test_for_get_all_songs_with_empty_list """
        with self.client:
            response = self.client.get('/getlist')
            data = json.loads(response.data.decode())
            self.assertTrue(data['status'] == 'fail')
            self.assertTrue(data['message'] == 'songlist is empty')
            self.assertTrue(response.content_type == 'application/json')
            self.assertEqual(response.status_code, 409)

    def test_clear(self):
        """ test_for_get_all_songs """
        with self.client:
            post = self.client.post('/add', 
                data=json.dumps(dict(
                    title = 'Blitzkrieg bop - The Ramones',
                    year = 1976)),
                content_type = 'application/json')
            response = self.client.delete('/clear')
            data = json.loads(response.data.decode())
            self.assertTrue(data['status'] == 'success')
            self.assertTrue(data['message'] == 'all list cleared')
            self.assertTrue(response.content_type == 'application/json')
            self.assertEqual(response.status_code, 200)
            response = self.client.get('/getlist')
            data = json.loads(response.data.decode())
            self.assertTrue(data['status'] == 'fail')
            self.assertTrue(data['message'] == 'songlist is empty')
            self.assertTrue(response.content_type == 'application/json')
            self.assertEqual(response.status_code, 409)

if __name__ == '__main__':
    unittest.main()